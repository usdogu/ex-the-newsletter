defmodule ExTheNewsletter.Repo.Migrations.CreateFeeds do
  use Ecto.Migration

  def change do
    create table(:feeds) do
      add :reference, :string, null: false
      add :title, :string, null: false

      timestamps()
    end

    create unique_index(:feeds, [:reference])
  end
end
