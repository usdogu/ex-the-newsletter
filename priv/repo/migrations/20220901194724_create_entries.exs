defmodule ExTheNewsletter.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries) do
      add :reference, :string, null: false
      add :feed_id, references(:feeds)
      add :title, :string, null: false
      add :author, :string, null: false
      add :content, :string, null: false

      timestamps()
    end
  end
end
