<h1 align="center">Ex the Newsletter!</h1>
<h3 align="center">Convert email newsletters into Atom feeds</h3>
<p align="center">
</p>

<h2 align="center">Support the original author</h2>
<h3 align="center">
<a href="https://patreon.com/leafac">Patreon (recurring)</a> ·
<a href="https://paypal.me/LeandroFacchinetti">PayPal (one-time)</a>
</h3>
<h2 align="center">Support me</h2>
<h3 align="center">
   <a href="https://github.com/sponsors/usdogu">Github sponsors</a>
   <p>BNB: 0xF5732Cc739eA063A78bC3D69923951C4608359B9</p>
   <p>XMR: 4ArogbFEq49DhBY611eUn2HM88ZiS1AEv9DQz1SyCN9LQNXFhhet7XLUcifjCZgiSPQkuMf1PHECKcmdLyeqP2FTBMTDxXu</p>
</h3>

## Info
This project is a port of leafac's excellent project [Kill The Newsletter!](https://github.com/leafac/kill-the-newsletter/) please consider donating to him if you want to donate

### Hosted Version
Hosted version is not available at this moment due to lack of money

### Self-Host

You may run Ex the Newsletter! on your own servers if you wish. This guarantees the utmost privacy, and it’s also a fun system adminstration project. Ex the Newsletter! strikes a good balance between being relatively easy to self-host and being non-trivial at the same time, because it is a web application as well as an email server.

#### Test on Your Machine

The best way to start self-hosting is to test Ex the Newsletter! on your machine. You can’t really use Ex the Newsletter! if it’s running on your machine because Ex the Newsletter!’s email server must be accessible from the internet to receive email and most likely your internet service provider blocks this kind of connection to prevent spam. Still, this is a good first step to get your feet wet by downloading and running Ex the Newsletter! for the first time.

Download the [latest release](https://codeberg.org/usdogu/ex-the-newsletter/releases/latest) and give it a try. You may send test emails using [curl](https://curl.se) by creating a file like the following:

`email.txt`

```
From: Publisher <publisher@example.com>
To: ru9rmeebswmcy7wx@localhost
Subject: Test email with HTML
Date: Sat, 13 Mar 2021 11:30:40

<p>Some HTML</p>
```

And then running the following command:

```
$ curl smtp://localhost:2525 --mail-from publisher@example.com --mail-rcpt ru9rmeebswmcy7wx@localhost --upload-file email.txt
```

Remember to change the `ru9rmeebswmcy7wx` in the example above to the appropriate email address for your Ex the Newsletter! test inbox.

#### Pre-Requisites

To install Ex the Newsletter! on your own server you’ll need:

1. A domain (for example, `ex-the-newsletter.com`). I buy domains at [Namecheap](https://www.namecheap.com).
2. A DNS server. I use the DNS server that comes with the domain I bought at Namecheap (and they even provide free DNS service for domains bought elsewhere).
3. A server. I rent a $6/month [DigitalOcean](https://www.digitalocean.com) droplet created with the following configuration:

   |                        |                                                                      |
   | ---------------------- | -------------------------------------------------------------------- |
   | **Distributions**      | Ubuntu 20.04 (LTS)                                                   |
   | **Plan**               | Share CPU · Regular Intel · $5/mo                                    |
   | **Datacenter region**  | I use New York 1, but you should use whatever is closest to you      |
   | **Additional options** | IPv6 & Monitoring                                                    |
   | **Authentication**     | SSH keys                                                             |
   | **Hostname**           | Your domain, for example, `ex-the-newsletter.com`                  |
   | **Backups**            | Enabled (that’s what makes the $5/month plan actually cost $6/month) |

   I also like to assign the droplet a **Floating IP** because it allows me to destroy and create droplets without having to change the DNS and wait for the DNS propagation to happen.

   This is the cheapest DigitalOcean offering, and yet it has managed Kill the Newsletter!’s traffic for years, even when it occasionally receives extra attention, for example, when it makes the front page of HackerNews.

#### DNS Configuration

This is where you associate domains to servers. For example, you associate `ex-the-newsletter.com` to the DigitalOcean droplet on which Ex the Newsletter! runs.

| Type    | Host  | Value                                               |
| ------- | ----- | --------------------------------------------------- |
| `A`     | `@`   | The (Floating) IP address of the server             |
| `Alias` | `www` | Your domain, for example, `ex-the-newsletter.com` |
| `MX`    | `@`   | Your domain, for example, `ex-the-newsletter.com` |

#### Running Ex the Newsletter!

SSH into the server:

```console
[your machine] $ ssh root@ex-the-newsletter.com
[the server] # mkdir ex-the-newsletter && cd ex-the-newsletter
[the server] # export SECRET_KEY_BASE=$(mix phx.gen.secret)
[the server] # MIX_ENV=prod mix do deps.get --only prod, compile, assets.deploy, phx.gen.release, release
[the server] # _build/prod/rel/ex_the_newsletter/bin/migrate
[the server] # _build/prod/rel/ex_the_newsletter/bin/server
```


#### Maintenance

All the data is stored under the same directory as the Ex The Newsletter! as a [SQLite](https://sqlite.org) database. If you ever have to migrate to a different server, just take it with you.

## API
POST to domain.com/api with {"feed_name": "feed name"}. It will return a feed adress and a mail.
