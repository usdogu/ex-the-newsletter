defmodule ExTheNewsletter.Repo do
  use Ecto.Repo,
    otp_app: :ex_the_newsletter,
    adapter: Ecto.Adapters.SQLite3

  alias ExTheNewsletter.{Feed, Entry}
  import Ecto.{Changeset, Query}

  def create_feed(attrs) do
    %Feed{}
    |> Feed.changeset(attrs)
    |> insert()
  end

  def create_entry(feed, attrs) do
    %Entry{}
    |> Entry.changeset(attrs)
    |> put_assoc(:feed, feed)
    |> IO.inspect()
    # auto update feed's update date whenever a entry arrives
    |> prepare_changes(fn prepared_changeset ->
      repo = prepared_changeset.repo
      feed_id = get_change(prepared_changeset, :feed_id)

      from(f in Feed,
        where: f.id == ^feed_id
      )
      |> repo.update_all(set: [updated_at: DateTime.utc_now()])

      prepared_changeset
    end)
    |> IO.inspect()
    |> insert()
  end

  @spec get_feed_by_reference(String.t()) :: Feed
  def get_feed_by_reference(reference) do
    get_by(Feed, reference: reference)
  end

  @spec get_entry_by_reference(String.t()) :: Entry
  def get_entry_by_reference(reference) do
    get_by(Entry, reference: reference)
  end

  def get_all_entries_of_feed(feed) do
    all(Ecto.assoc(feed, :entries))
  end
end
