defmodule ExTheNewsletter.Entry do
  use Ecto.Schema
  import Ecto.Changeset
  alias ExTheNewsletter.Feed

  schema "entries" do
    field :reference, :string
    field :author, :string
    field :content, :string
    belongs_to :feed, Feed
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(entry, attrs) do
    entry
    |> cast(attrs, [:feed_id, :title, :author, :content, :reference])
    |> validate_required([:title, :author, :content])
  end
end
