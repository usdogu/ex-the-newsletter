defmodule ExTheNewsletter.SmtpServer do
  @behaviour Pique.Behaviours.Handler
  alias ExTheNewsletter.Repo

  def handle(%{body: bod} = state) do
    %{body: body, headers: %{"from" => from, "subject" => subject, "to" => rcpt}} =
      Mail.Parsers.RFC2822.parse(bod)

    from = elem(from, 1)
    rcpt = List.first(rcpt)
    reference = String.split(rcpt, "@") |> List.first()
    feed = Repo.get_feed_by_reference(reference)
    entry_ref = Nanoid.generate(16, "abcdefghijklmnopqrstuvwxyz0123456789")

    Repo.create_entry(feed, %{
      reference: entry_ref,
      author: from,
      content: body,
      title: subject,
      feed_id: feed.id
    })

    {:ok, state}
  end
end
