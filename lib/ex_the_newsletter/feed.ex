defmodule ExTheNewsletter.Feed do
  use Ecto.Schema
  import Ecto.Changeset
  alias ExTheNewsletter.Entry

  schema "feeds" do
    field :reference, :string
    field :title, :string
    has_many :entries, Entry
    timestamps()
  end

  @doc false
  def changeset(feed, attrs) do
    feed
    |> cast(attrs, [:reference, :title])
    |> validate_required([:reference, :title])
    |> unique_constraint(:reference)
  end
end
