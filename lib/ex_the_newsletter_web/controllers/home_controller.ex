defmodule ExTheNewsletterWeb.HomeController do
  use ExTheNewsletterWeb, :controller
  require Logger
  alias ExTheNewsletter.Repo

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def new(conn, %{"newsletter_name" => newsletter_name}) do
    reference = Nanoid.generate(16, "abcdefghijklmnopqrstuvwxyz0123456789")
    welcome_title = "“#{newsletter_name}” inbox created"
    Repo.create_feed(%{reference: reference, title: newsletter_name})

    render(conn, "new.html",
      newsletter_name: newsletter_name,
      reference: reference,
      welcome_title: welcome_title
    )
  end
end
