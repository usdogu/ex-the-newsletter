defmodule ExTheNewsletterWeb.AlternateController do
  use ExTheNewsletterWeb, :controller
  alias ExTheNewsletter.Repo

  def index(conn, %{"entry_reference" => entry_reference}) do
    entry = Repo.get_entry_by_reference(entry_reference)

    conn
    |> put_resp_content_type("text/html")
    |> send_resp(200, entry.content)
  end
end
