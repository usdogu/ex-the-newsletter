defmodule ExTheNewsletterWeb.ApiController do
  use ExTheNewsletterWeb, :controller
  alias ExTheNewsletter.Repo

  def create(conn, %{"feed_name" => feed_name}) do
    reference = Nanoid.generate(16, "abcdefghijklmnopqrstuvwxyz0123456789")
    feed = Repo.create_feed(%{reference: reference, title: feed_name})
    mail = reference <> "@" <> Application.fetch_env!(:ex_the_newsletter, :url)
    feed_adress = Application.fetch_env!(:ex_the_newsletter, :url) <> "/feeds/" <> reference
    json(conn, %{mail: mail, feed_address: feed_adress})
  end
end
