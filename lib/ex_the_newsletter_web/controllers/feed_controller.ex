defmodule ExTheNewsletterWeb.FeedController do
  use ExTheNewsletterWeb, :controller
  alias Atomex.{Feed, Entry}
  alias ExTheNewsletter.Repo
  require Logger

  def index(conn, %{"feed_reference" => feed_reference}) do
    feed = Repo.get_feed_by_reference(feed_reference)
    entries = Repo.get_all_entries_of_feed(feed)
    url = Application.fetch_env!(:ex_the_newsletter, :url)

    xml =
      Feed.new(
        "urn:ex-the-newsletter:#{feed_reference}",
        DateTime.from_naive!(feed.updated_at, "Etc/UTC"),
        feed.title
      )
      |> Feed.author("Ex the Newsletter!")
      |> Feed.link("#{url}/feeds/#{feed_reference}",
        rel: "self",
        type: "application/atom+xml"
      )
      |> Feed.link("https://#{url}", rel: "alternate", type: "text/html")
      |> Feed.subtitle(
        "Ex The Newsletter! Inbox:\n #{feed_reference}@#{url} ->\nhttps://#{url}/feeds/#{feed_reference}"
      )
      |> Feed.entries(Enum.map(entries, &get_entry/1))
      |> Feed.build()
      |> Atomex.generate_document()

    conn
    |> put_resp_content_type("application/atom+xml")
    |> send_resp(200, xml)
  end

  defp get_entry(
         _entry = %ExTheNewsletter.Entry{
           inserted_at: created_at,
           reference: reference,
           title: title,
           author: author,
           content: content
         }
       ) do
    Entry.new(
      "urn:ex-the-newsletter:#{reference}",
      DateTime.from_naive!(created_at, "Etc/UTC"),
      title
    )
    |> Entry.author(author)
    |> Entry.content(content)
    |> Entry.build()
  end
end
