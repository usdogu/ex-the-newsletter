defmodule ExTheNewsletterWeb.Router do
  use ExTheNewsletterWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {ExTheNewsletterWeb.LayoutView, :root})
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", ExTheNewsletterWeb do
    pipe_through(:browser)

    get("/", HomeController, :index)
    post("/", HomeController, :new)
    get("/feeds/:feed_reference", FeedController, :index)
    get("/alternates/:entry_reference", AlternateController, :index)
  end

  # Other scopes may use custom stacks.
  scope "/api", ExTheNewsletterWeb do
    pipe_through(:api)
    post("/", ApiController, :create)
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through(:browser)

      live_dashboard("/dashboard", metrics: ExTheNewsletterWeb.Telemetry)
    end
  end
end
