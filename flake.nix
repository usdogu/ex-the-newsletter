{
  description = "Ex the Newsletter development environment";
  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/nixos-unstable"; };
    flake-utils = { url = "github:numtide/flake-utils"; };
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        inherit (nixpkgs.lib) optional;
        pkgs = import nixpkgs { inherit system; };
        elixir = pkgs.beam.packages.erlang.elixir;
        elixir-ls = pkgs.beam.packages.erlang.elixir_ls;
        locales = if pkgs.stdenv.hostPlatform.libc == "glibc" then
          pkgs.glibcLocales.override {
            allLocales = false; # Only en-US utf8
          }
        else
          pkgs.locale;
      in {
        devShells.default = pkgs.mkShell {
          buildInputs = [ elixir elixir-ls locales ]
            ++ pkgs.lib.optionals pkgs.stdenv.isLinux [ pkgs.inotify-tools ]
            ++ pkgs.lib.optionals pkgs.stdenv.isDarwin
            (with pkgs.darwin.apple_sdk.frameworks; [ Cocoa CoreServices ]);
        };
      });
}
